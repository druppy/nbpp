# This is nb++, a nuts-and-bolts C++ toolkit.

This project holds an old legazy library that handle a lot of the things need
to make simple network daemon's.

Most of the functionality are now covered in C++11 std, and only the
network part of this lib remaine relevant.

The lib will be maintained to keep it functional or relevant for a
few projects of mine, but other than that it will not change much.